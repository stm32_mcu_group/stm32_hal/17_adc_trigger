//
// Created by Csurleny on 5/1/2022.
//

#ifndef INC_11_I2C_MASTER_DISPLAY_FONTS_H
#define INC_11_I2C_MASTER_DISPLAY_FONTS_H

#include <stdint.h>


// Font structure definition
typedef struct{
    uint8_t fWidth;         // Font width (px)
    uint8_t fHeight;        // Font height (px)
    const uint16_t *fData;   // Font data array pointer
}fDef_t;

// String length and height
typedef struct{
  uint16_t sLength;         // String length (px)
  uint16_t sHeight;         // String height (px)
}fSize_t;

// These fonts are deffined in the .c file
extern fDef_t font_7x10;
extern fDef_t font_11x18;
extern fDef_t font_16x26;

char* fGet_StringSize(char *str, fSize_t *sizeStruct, fDef_t *Font);

#endif //INC_11_I2C_MASTER_DISPLAY_FONTS_H
